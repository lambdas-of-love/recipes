# Ingredients

* 4 tablespoons butter
* 3 garlic cloves
* 1 diced onion
* 8 ounces mushrooms, thinly sliced
* Kosher salt and freshly ground black pepper, to taste
* 2 cups chicken broth
* 1 cup rice
* 1/4 teaspoon dried thyme
* 2 cups baby spinach
* 3/4 cup frozen peas
* 1/4 cup grated Parmesan or Romano


# Directions

* Turn instant pot on sauttee mode and toss in half the butter, onions, and garlic.
* Once the onions have been in there and are translucent (3-4 min) toss in the mushrooms, season with salt and pepper.
* After the mushrooms have been in for 4 min add in the chicken stock, thyme, and rice.
* Set the instant pot pressure to HIGH and set the timer for 6 minutes.
* After 6 minutes do a quick release on the instant pot.
* Add in spinach and remaining butter and stir till wilted.
* Add in peas and parmesan.
