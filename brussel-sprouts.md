# Oven

## Ingredients

* Brussel Sprouts
* Olive Oil
* Balsamic Vinegar

## Instructions

Cut the ends off the sprouts, then cut them in half. Place them into a bowl
and drizzle olive oil on them and then mix them till they're lightly covered.

Bake in an oven at 410 degrees fahrenheit till they are at a desired level
of char. Be sure to turn them and mix them up every once in a while.

Drizzle with balsamic vinegar and maybe a small amount of salt if you're feeling
it.


# Skillet

Heavily oil a frying pan.

Slice brussel sproutes in half and place halfs down on skillet. Heat on medium covered untill done.

Add course ground pepper, balsamic vinegar and shredded romano cheese.
