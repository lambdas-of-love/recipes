
# Ingredients

* 5-7 Boneless skinless chicken thighs
* 6 Tbsp Lime Juice (or 2 limes)
* 1/4 cup olive oil
* 1 tsp salt
* 2 tsp sugar
* 1 tsp garlic powder
* 1 tsp chilli powder
* 1/4 tsp italian seasoning

# Steps

1. Trim the fat off the thighs.
2. Add everything into a ziplock bag and mix well. Let sit in the refrigerator 2-3 hours.
3. Remove the bag from the refrigerator and let sit for 20 minutesish to warm up slightly.
4. Dry the thicks off with a paper towel.
5. Get a cast iron skillet and put some high heat oil (vegetable/peanut/whatever) in it to grease it.
6. Get the skillet ripping hot, like almost to the point where it starts to smoke.
7. Cook thighs untill slightly charred (should be about 5 minutes per side). Things are going to get smokey so
open a window or something.
8. Take them out of the skillet and don't fucking touch them for 10 minutes, I mean it.
9. Cut them up, add them to tacos.
