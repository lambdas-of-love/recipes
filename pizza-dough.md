# Sourish Pizza Dough

## Ingredients

* 500g Bread Flour
* 16g salt
* 1g active dry yeast
* 350 ml water

## Directions

1. Mix all dry ingredients together then slowly add water into a shaggy dough.

2. Once there's no dry clumps cover in plastic wrap and let sit unrefridgerated for 24 hours.

3. Cut dough into 4 balls, each one should make a small to medium-ish size pizza. Freeze any that you don't need.


That's it.
