* 1-2 skirt steak (enough for maybe 1-2 people per steak)


# The Marinade

* 2 limes juiced
* 4 cloves of garlic crushed
* 1/2 cup orange juice
* 1 cup chopped cilantro (if you hate cilantro just leave it out)
* 1/2 teaspoon salt
* 1/4 teaspoon black pepper
* 1/4 cup olive oil
* 2 tablespoons white vinegar

# The Not Marinade

* 1 white onion
* leftover cilantro to taste
* lime juice to taste
* corn tortillas
* hot sauce


# Steps

Mix ingredients together the marinade, let skirt steaks marinade in the refridgerator
for at least 2 hours (or overnight).

Get a grill really really hot, like 500 degrees F. Cook the steak for like 3-4 min on each side
then set aside to rest for at least 10 minutes. Cut into cubes.

Make the salsa by dicing the onion and mixing in cilantro and lime juice to taste.

Boom.
