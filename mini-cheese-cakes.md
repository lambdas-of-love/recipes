
# The Hardware

* A muffin tin
* Paper cupcake things.

# The Crust

* 1 cup graham cracker crumbs
* 4 tablespoons unsalted butter, melted
* 2 tablespoons granulated sugar

# The Filling

* 16 ounces cream cheese, softened
* ½ cup sour cream
* ¼ cup granulated sugar
* 2 eggs
* 1 teaspoon vanilla extract


# The Instructions


1. Preheat oven to 325 degrees.
2. Line a regular size muffin pan with paper liners.
3. Combine graham cracker, butter and sugar in a small bowl. Texture should be similar to wet sand. Divide crust evenly into the bottom of the lined muffin tin.
4. Bake for 5-6 minutes or until golden brown.
5. Take out of the oven and cool completely.
6. Meanwhile assemble the cheesecake filling. Beat cream cheese in a stand mixer with the paddle attachment.
7. Add in sour cream, sugar, eggs and vanilla. Mix until combined. Make sure to scrap the sides of the bowl.
8. Pour cheesecake mixture into cooled muffin tin. It will be about 2 tablespoons of filling each. Fill almost all the way to the top.
9. Place in the oven and bake for 20 minutes or until the cheesecakes are set. They will still jiggle a bit. Do not over cook them. If they start to crack they are getting over cooked.
10. Allow them to cool in the muffin tin completely. Place in the refrigerator to chill and serve cold with your favorite toppings.

