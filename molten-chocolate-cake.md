Requirements:
* 3/4 cup bitter sweet chocolate
* 1 stick butter
* 3 egg yolks
* 2 eggs
* 1/2 cup flour
* 1 teaspoon vanilla
* 1/2 cup confectioners sugar
* 4 ramekins


1. Melt chocolate and butter in a double boiler.
2. Quickly whisk in the eggs + yolks.
3. Whisk in sugar + vanilla.
4. Whisk in flour.
5. Pour batter into ramekins.
6. Sprinkle salt on top (optional).
7. Cook in oven at 425 for 11 minutes (in 4 rammacins).
8. The result should be a partially cooked cake that is liquid in the center.
9. Serve immediately with ice cream.
