# Chicken Thighs

## Ingredients

* 1 tsp Salt
* 1 tsp Vegetable oil
* 3 bone in, with skin chicken thighs

## Directions

1. Dry off the chicken thighs
2. Salt the chicken thighs liberally.
3. Heat up a cast iron pan with the oil in it.
4. Sear the thighs skin side down for a couple minutes or untill they are crispy.
5. place thighs in a 425 degree F oven skin side up for 15-20 minutes.
6. Let rest for 10 minutes.
