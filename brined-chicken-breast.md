# Ingredients

* Chicken breast (3-4)
* 1/4th cup salt
* 4 cups warm water
* 1 Tbsp whole black peppercorn
* 1 Tsp whole mustard seed


1. Add all the ingredients into the water, make sure the salt is
dissolved.

2. Let sit in refridgerator for 30 min (any longer and it will be saltier).

3. Preheat oven to 420.

4. Put a dry rub of your choice + olive oil on the chicken.

5. Heat for 20 minutes, let rest for 10 minutes.
