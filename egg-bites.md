# Egg Bites

* 1 red bell pepper
* 6-7 eggs
* 1 cup chopped spinach
* 1 cup shredded cheese
* Salt & Pepper
* Cottage cheese 1/4 cup
* 1/4 cup water
* crushed garlic (as much as you want)

400 degree oven, 30 minutes in some silicone muffin pans that have been greased with olive oil.
