# Fermented Sour Pickles

* 66 grams salt
* 8 cups water
* Several small cucumbers
* 1 Tsb dried dill
* 1 Tsb black peppercorns
* 1 Jalapeno
* 1 Tsb mustard seed
* 1 Tsp celery salt
* 1 Large glass jar
* 3 garlic cloves

# Directions

1. Crush the garlic cloves.
2. Remove the seeds from the jalapeno.
3. Add everything to the jar, ensure all the salt dissolves.
4. Mix everything and let sit for 3 days at room temp, periodically open the jar to remove gasses.
5. Place in refridgerator for 7 days.

